package main

import "net/http"

type UserEndpointFunctions interface {
	CreateEndpoint(w http.ResponseWriter, r *http.Request)
	AllEndpoint(w http.ResponseWriter, r *http.Request)
	FindEndpoint(w http.ResponseWriter, r *http.Request)
	UpdateEndpoint(w http.ResponseWriter, r *http.Request)
	DeleteEndpoint(w http.ResponseWriter, r *http.Request)
}