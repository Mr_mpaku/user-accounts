package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func CreateDeveloperEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Create Developer not implemented yet !")
}

func AllDevelopersEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Get All Developers not implemented yet !")
}

func FindDeveloperEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Find a certain Developer not implemented yet !")
}

func UpdateDeveloperEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Update a certain Developer not implemented yet !")
}

func DeleteDeveloperEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Delete a certain not implemented yet !")
}

// ----------------- PROJECT MANAGER FUNCTOIONS ----------------------------

func CreateProjectManagerEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Create PM not implemented yet !")
}

func AllProjectManagersEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Get All PMs not implemented yet !")
}

func FindProjectManagerEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func UpdateProjectManagerEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func DeleteProjectManagerEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

// ----------------- CLIENT FUNCTIONS ---------------------------------

func CreateClientEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Create client not implemented yet !")
}

func AllClientsEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Get all clients not implemented yet !")
}

func FindClientEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func UpdateClientEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func DeleteClientEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

// Defining the HTTP routes - Endpoints
func main() {
	router := mux.NewRouter()

	// Developer Endpoints
	router.HandleFunc("/developer", AllDevelopersEndpoint).Methods("GET")
	router.HandleFunc("/developer/", CreateDeveloperEndpoint).Methods("POST")
	router.HandleFunc("/developer/{id}", FindDeveloperEndpoint).Methods("GET")
	router.HandleFunc("/developer/{id}", UpdateDeveloperEndpoint).Methods("PUT")
	router.HandleFunc("/developer/{id}", DeleteDeveloperEndpoint).Methods("DELETE")

	// Project Manager Endpoints
	router.HandleFunc("/projectManager", AllProjectManagersEndpoint).Methods("GET")
	router.HandleFunc("/projectManager/", CreateProjectManagerEndpoint).Methods("POST")
	router.HandleFunc("/projectManager/{id}", FindProjectManagerEndpoint).Methods("GET")
	router.HandleFunc("/projectManager/{id}", UpdateProjectManagerEndpoint).Methods("PUT")
	router.HandleFunc("/projectManager/{id}", DeleteProjectManagerEndpoint).Methods("DELETE")

	// Clients Endpoints
	router.HandleFunc("/client", AllClientsEndpoint).Methods("GET")
	router.HandleFunc("/client/", CreateClientEndpoint).Methods("POST")
	router.HandleFunc("/client/{id}", FindClientEndpoint).Methods("GET")
	router.HandleFunc("/client/{id}", UpdateClientEndpoint).Methods("PUT")
	router.HandleFunc("/client/{id}", DeleteClientEndpoint).Methods("DELETE")

	// Checking if PORT 3000 is open and can serve resources on it
	if err := http.ListenAndServe(":3000", router); err != nil {
		log.Fatal(err)
	}
}
