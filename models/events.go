package models

import "gopkg.in/mgo.v2/bson"

type Event struct {
	ID          bson.ObjectId    `bson:"_id" json:"id"`
	Title       string           `bson:"title" json:"title"`
	Type        string           `bson:"type" json:"type"`
	Date        string           `bson:"date" json:"date"`
	Description string           `bson:"description" json:"description"`
	StartTime   string           `bson:"startTime" json:"startTime"`
	EndTime     string           `bson:"endTime" json:"endTime"`
	Submissions []bson.ObjectId  `bson:"submissions" json:"submissions"`
}

type Submission struct {
    ID              []bson.ObjectId  `bson:"_id" json:"id"`
    Title           string           `bson:"title" json:"title"`
    DEvIds          []bson.ObjectId  `bson:"devIds" json:"devIds"`
    Description     string           `bson:"description" json:"description"`
    Link            string           `bson:"link" json:"link"`
}