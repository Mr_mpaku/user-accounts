package models

import "gopkg.in/mgo.v2/bson"

type OnlineProfiles struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	DeveloperID bson.ObjectId     `bson:"developerId" json:"developerId"`
	Blog        string        `bson:"blog" json:"blog"`
	Github      string        `bson:"github" json:"github"`
	GitLab      string        `bson:"gitlab" json:"gitlab"`
	BitBucket   string        `bson:"bitBucket" json:"bitBucket"`
	Personal    string        `bson:"personal" json:"personal"`
	LinkedIn    string        `bson:"linkedIn" json:"linkedIn"`
	Other       string        `bson:"other" json:"other"`
}
