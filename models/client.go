package models

import "gopkg.in/mgo.v2/bson"

type Client struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	CompanyName string        `bson:"companyName" json:"companyName"`
	Slogan      string        `bson:"slogan" json:"slogan"`
	Email       string        `bson:"email" json:"email"`
	Password    string        `bson:"password" json:"password"`
	Description string        `bson:"description" json:"description"`
	ContactNo   string        `bson:"contactNo" json:"contactNo"`
	ProfilePic  string        `bson:"profilePic" json:"profilePic"`
	Type        string        `bson:"type" json:"type"`
	Address     []Address     `bson:"address" json:"address"`
	Website     string        `bson:"website" json:""`
	Resources   string        `bson:"resource" json:"resource"`
}
