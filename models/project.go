package models

import "gopkg.in/mgo.v2/bson"

type Project struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	ProjectName string        `bson:"projectName" json:"projectName"`
	Description string        `bson:"description" json:"description"`
	TechStack   string        `bson:"techStack" json:"techStack"`
	Price       int           `bson:"price" json:"price"`
	Link        string        `bson:"link" json:"link"`
	Logo        string        `bson:"logo" json:"logo"`
	Cluster     bson.ObjectId       `bson:"cluster" json:"cluster"`
	Client      bson.ObjectId        `bson:"client" json:"client"`
	Status      string        `bson:"status" json:"status"`
}
