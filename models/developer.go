package models

import "gopkg.in/mgo.v2/bson"

type Developer struct {
	ID                  bson.ObjectId   	 `bson:"_id" json:"id"`
	Name                string          	 `bson:"name" json:"name"`
	Lastname            string          	 `bson:"name" json:"lastname"`
	Password            string          	 `bson:"password" json:"password"`
	Email               string          	 `bson:"email" json:"email"`
	UniversityName      string          	 `bson:"universityName" json:"universityName"`
	YearOfStudy         string          	 `bson:"yearOfStudy" json:"yearOfStudy"`
	Description         string          	 `bson:"description" json:"description"`
	Interests           []string        	 `bson:"interests" json:"interests"`
	ContactNo           string          	 `bson:"contactNo" json:"contactNo"`
	Skills              []string        	 `bson:"skills" json:"skills"`
	TypeOfDeveloper     string          	 `bson:"typeOfDeveloper" json:"typeOfDeveloper"`
	Cluster             Cluster         	 `bson:"cluster" json:"cluster"`
	ProjectsCompleted   []bson.ObjectId      `bson:"projectsCompleted" json:"projectsCompleted"`
	ChallengesCompleted []ChallengeCompleted `bson:"challengesCompleted" json:"challengesCompleted"`
	ToursAttended       []Tour        		 `bson:"toursAttended" json:"toursAttended"`
	IntershipsDone      []Internship    	 `bson:"internshipsDone" json:"internshipsDone"`
	OverallZaions       []Zaions        	 `bson:"overallZaions" json:"overallZaions"`
	EventsAttended		[]bson.ObjectId		 `bson:"eventsAttended" json:"eventsAttended"`
	ProfilePic          string        		 `bson:"profilePic" json:"profilePic"`
}

type ChallengeCompleted struct {
	ChallengeId bson.ObjectId `bson:"_id" json:"id"`
	ChallengeSubmission string `bson:"challengesCompleted" json:"challengesCompleted"`
}