package models

import "gopkg.in/mgo.v2/bson"

type ProjectManager struct {
	ID                bson.ObjectId `bson:"_id" json:"id"`
	Name              string        `bson:"name" json:"name"`
	Lastname          string        `bson:"lastname" json:"lastname"`
	Email             string        `bson:"email" json:"email"`
	Password          string        `bson:"password" json:"password"`
	Description       string        `bson:"description" json:"description"`
	ContactNo         string        `bson:"contactNo" json:"contactNo"`
	NumberOfProjects  int           `bson:"numberOfProjects" json:"numberOfProjects"`
	ProjectsCompleted []bson.ObjectId     `bson:"projectsCompleted" json:"projectsCompleted"`
	ProfilePic        string        `bson:"profilePic" json:"profilePic"`
}
