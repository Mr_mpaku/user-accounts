package models

import "gopkg.in/mgo.v2/bson"

type PersonalExperience struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	DeveloperID bson.ObjectId     `bson:"developerId" json:"developerId"`
	Title       string        `bson:"title" json:"title"`
	Description string        `bson:"description" json:"description"`
	Link        string        `bson:"link" json:"link"`
}
