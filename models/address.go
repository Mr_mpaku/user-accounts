package models

import "gopkg.in/mgo.v2/bson"

type Address struct {
	ID            bson.ObjectId `bson:"_id" json:"id"`
	StreetAddress string        `bson:"streetAddress" json:"streetAddress"`
	Postcode      int           `bson:"postCode" json:"postCode"`
	City          string        `bson:"city" json:"city"`
	Province      string        `bson:"province" json:"province"`
	Country       string        `bson:"country" json:"country"`
}
