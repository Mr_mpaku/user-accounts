package models

import "gopkg.in/mgo.v2/bson"

type Challenge struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	Title       string        `bson:"title" json:"title"`
	Date        string        `bson:"date" json:"date"`
	Description string        `bson:"description" json:"description"`
	Notes       string        `bson:"notes" json:"notes"`
	Zaions      int           `bson:"zaions" json:"zaions"`
	Level       string        `bson:"level" json:"level"`
	Topic       string        `bson:"topic" json:"topic"`
	Duration    string        `bson:"duration" json:"duration"`
	PostedBy    Client        `bson:"postedBy" json:"postedBy"`
	Role        string        `bson:"role" json:"role"`
}
