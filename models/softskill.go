package models

type SoftSkill struct {
	Type        string `bson:"type" json:"type"`
	SkillZaions int    `bson:"skillZaions" json:"skillZaions"`
}
