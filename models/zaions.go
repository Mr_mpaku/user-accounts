package models

import "gopkg.in/mgo.v2/bson"

type Zaions struct {
	ID                bson.ObjectId `bson:"_id" json:"id"`
	ProjectsZaions    int           `bson:"projectZaions" json:"projectZaions"`
	ChallengesZaoins  int           `bson:"challengesZaions" json:"challengesZaions"`
	ToursZaions       int           `bson:"toursZaions" json:"toursZaions"`
	InternshipsZaions int           `bson:"internshipsZaions" json:"internshipsZaions"`
	HackathonsZaions  int           `bson:"hackathonsZaions" json:"hackathonsZaions"`
	SoftSkillsZaions  []SoftSkill    `bson:"softSkillsZaions" json:"softSkillsZaions"`
}
