package models

import "gopkg.in/mgo.v2/bson"

type Cluster struct {
	ID             bson.ObjectId  `bson:"_id" json:"id"`
	ClusterName    string         `bson:"clusterName" json:"clusterName"`
	NumberOfDevs   int            `bson:"numberOfDevs" json:"numberOfDevs"`
	Developers     []bson.ObjectId    `bson:"developers" json:"developers"`
	Projects       []bson.ObjectId      `bson:"projects" json:"projects"`
	ProjectManager bson.ObjectId `bson:"projectManager" json:"projectManager"`
	ClusterIcon    string         `bson:"clusterIcon" json:"clusterIcon"`
}
